@extends('layouts.application')
@section('title')
ยินดีต้อนรับสู่
@stop

@section('meta')
    <meta name="description" content="{{ Setting::SITE_META() }}">
    <meta name="author" content="Wattana Bumrungsawat">
    <link rel="canonical" href="http://www.bumrungsawat.com/2014/" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ Setting::SITE_TITLE() }}" />
    <meta property="og:description" content="{{ Setting::SITE_META() }}" />
    <meta property="og:url" content="http://www.bumrungsawat.com/2014/" />
    <meta property="og:site_name" content="Bumrungsawat" />
    <meta property="article:publisher" content="https://www.facebook.com/hansamurano" />
    <meta property="og:image" content="http://www.bumrungsawat.com/2014/assets/images/logo.png" />
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@hansamurano"/>
    <meta name="twitter:domain" content="Bumrungsawat"/>
    <meta name="twitter:creator" content="/@hansamurano"/>
@stop

@section('style')
@stop
@section('main')
            
<div class="bg-content">
  <div class="container">
    <div class="row">
          <div class="span12"> 
        <!--============================== slider =================================-->
        <div class="flexslider">
            <?php $home_page = Page::where('slug', '=', 'home')->first(); ?>
          <ul class="slides">
            @if($home_page)
            @foreach($home_page->images as $image)
            <li> <img src="{{ url($image->image->url('original'))}}" alt=""  > </li>
            @endforeach
            @endif
          </ul>
            </div>
        <span id="responsiveFlag"></span>
        <div class="block-slogan">
            <h2>Welcome!</h2>
          <div>
            @if($home_page)
                {{ $home_page->content }}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
      
      <!--============================== content =================================-->
      
  <div id="content" class="content-extra">
    <div class="row-1">
      {{ View::make('latest_posts') }}
    <div class="container">
      <div class="row">
        <article class="span6"> 
          <h2>{{ $talk->title}}</h2>   
          <figure class="img-indent">   
            <div class=""> 
              <span class="f-img-wrap">
                <a href="{{ url(action('PostsController@show', $talk->slug)) }}" title="{{ $talk->title}}"><img src="{{ url($talk->cover->url('original')) }}" alt="Image 04 " class="image_fl img-responsive pull-left" style="max-width:120px;"></a>
              </span>     
            </div>  
          </figure> 
          <div class="inner-1 overflow extra">    
            <div class="txt-1">
              {{ $talk->summary}}
            <div class="overflow">      
              <ul class="list list-pad">        
                <li class="flow"><a href="http://www.bumrungsawat.com/2014/pages/photography" target="_new" class="f-link">รับบันทึกภาพแห่งความสุข</a>          </li>       
                <li class="flow nomr"><a href="http://www.bumrungsawat.com/2014/pages/package-photography" class="f-link">Package ถ่ายภาพนอกสถานที่</a>         </li>       
                <li class="flow"><a class="f-link" href="undefined">รับตัดเสื้อโปโล,เสื้อยืดราคาถูก</a>         </li>       
                <li class="flow nomr"><a class="f-link" href="undefined">บริการออกแบบบรรจุภัณฑ์ Logo</a>          </li>     
              </ul>   
            </div>  
            </div>
          </div>
        </article>
        <article class="span6">
          <h3>Latest photoshoots</h3>
          <ul class="list-photo">
            @foreach($images as $image)
            <li><a href="{{ url($image->image->url('original')) }}" class="magnifier" ><img src="{{ url($image->image->url('large')) }}" alt="" /></a></li>
            @endforeach
          </ul>
        </article>
      </div>
    </div>
  </div>
</div>

@stop            

@section('script')
@stop
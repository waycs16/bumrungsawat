<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/assets/images/favicon/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/assets/images/favicon/favicon.ico" type="image/x-icon">
    @yield('meta')

    <title>@yield('title') 
        | 
        {{ Setting::SITE_TITLE() }}
    </title>

    <!-- Bootstrap core CSS -->
    {{ HTML::style('assets/css/bootstrap.css') }}
    <!-- Font Awesome CSS -->
    {{ HTML::style('assets/css/responsive.css') }}
    <!-- STYLE CSS -->
    {{ HTML::style('assets/css/style.css') }}
    {{ HTML::style('assets/css/touchTouch.css') }}
    {{ HTML::style('assets/css/kwicks-slider.css') }}
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>


    <!-- Jquery Source -->
    {{ HTML::script('assets/js/jquery.js') }}
    <!-- Main Action Javascript -->
    {{ HTML::script('assets/js/superfish.js') }}
    {{ HTML::script('assets/js/jquery.flexslider-min.js') }}
    {{ HTML::script('assets/js/jquery.kwicks-1.5.1.js') }}
    {{ HTML::script('assets/js/jquery.easing.1.3.js') }}
    {{ HTML::script('assets/js/touchTouch.jquery.js') }}
    <script type="text/javascript">
    if($(window).width()>1024){
        document.write("<"+"script src='{{ url('assets/js/jquery.preloader.js') }}'></"+"script>");
    }  
    </script>

    <script>        
        jQuery(window).load(function() {   
        $x = $(window).width();        
        if($x > 1024)
        {           
            jQuery("#content .row").preloader();    }   
                 
             jQuery('.magnifier').touchTouch();         
            jQuery('.spinner').animate({'opacity':0},1000,'easeOutCubic',function (){jQuery(this).css('display','none')});  
        }); 
                
    </script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      {{ HTML::script('assets/js/html5shiv.js') }}
      {{ HTML::script('assets/js/respond.min.js') }}
    <![endif]-->
    @yield('style')
</head>
<header>
  <div class="container clearfix">
    <div class="row">
      <div class="span12">
        <div class="navbar navbar_">
          <div class="container">
            <h1 class="brand brand_"><a href="{{ url('/') }}"><img alt="" src="{{ url('/assets/images/logo.png') }}"> </a></h1>
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse_">Menu <span class="icon-bar"></span> </a>
            <div class="nav-collapse nav-collapse_  collapse">
              <ul class="nav sf-menu">
                <?php $menus = Menu::where('menu_id', '0')->orderBy('ordering', 'asc')->get(); ?>              
                 
                @if ($menus->count())
                    @foreach ($menus as $menu)
                        <?php $submenus = Menu::where('menu_id', $menu->id)->get(); 
                        $path = strpos( $menu->path, '#') > 0 ? substr($menu->path, 0, strpos( $menu->path, '#')) : $menu->path;  
                        if($path !== "/")
                            $active = str_contains(Request::url()."#", $path."#") ? ' class="active"' : '';                   
                        else
                            $active = Request::is($path."*") ? ' class="active"' : '';  

                        foreach ($submenus as $submenu) {
                            $path = strpos( $submenu->path, '#') > 0 ? substr($submenu->path, 0, strpos( $submenu->path, '#')) : $submenu->path;  
                            if($path !== "/")
                            {
                                if(str_contains(Request::url()."#", $path."#"))
                                {
                                    $active = ' class="active"';                   
                                    break;
                                }
                            }
                            else
                            {
                                if(Request::is($path."*"))
                                {
                                    $active = ' class="active"';                    
                                    break;
                                }
                            }
                        }
                        ?>
                        <li{{$active}}><a href="{{url($menu->path)}}" title="{{ $menu->title }}">{{ $menu->title }}</a>
                        @if($submenus->count() > 0 )
                            <ul>
                                @foreach ($submenus as $submenu)
                                <li><a href="{{url($submenu->path)}}" title="{{ $submenu->title }}">{{ $submenu->title }}</a>                                    
                                </li>
                                @endforeach 
                            </ul>
                        @endif
                        </li>                        
                    @endforeach 
                @endif  
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
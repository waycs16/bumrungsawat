@extends('layouts.application')

@section('title')
{{$page->title}}
@stop

@section('meta')
    <meta name="description" content="{{ Setting::SITE_META() }}">
    <meta name="author" content="Wattana Bumrungsawat">
    <link rel="canonical" href="http://www.bumrungsawat.com/2014/" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $page->title }}{{ Setting::SITE_TITLE() }}" />
    <meta property="og:description" content="{{ Setting::SITE_META() }}" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:site_name" content="Bumrungsawat" />
    <meta property="article:publisher" content="https://www.facebook.com/hansamurano" />
    <meta property="og:image" content="http://www.bumrungsawat.com/2014/assets/images/logo.png" />
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@hansamurano"/>
    <meta name="twitter:domain" content="Bumrungsawat"/>
    <meta name="twitter:creator" content="/@hansamurano"/>
@stop

@section('style')
<style>
{{$page->style}}
</style>
@stop

@section('main')
<div class="bg-content">
	<div id="content">
    	<div class="container">
          	<div class="row span10">
          	<article class="span12">
	        	<h3>&nbsp;</h3>
         	</article>
			{{$page->content}}
      		</div>
        </div>
  	</div>
</div>
@stop


@section('script')
@stop
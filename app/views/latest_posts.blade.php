<div class="container">
        <div class="row">
          <ul class="thumbnails thumbnails-1">
            <?php $category = Category::where('slug', '=', 'fashion')->first(); 
              $post = $category->posts()->orderBy('id','desc')->first();
            ?>
            <li class="span3">
                <div class="thumbnail thumbnail-1">
                <h3>{{ $category->name_th }}</h3>
                <img  src="{{ url($post->cover->url('large')) }}" alt="{{ $post->title}}">
                <section> <strong>{{ $post->title}} </strong>
                      <p>{{ $post->summary }}</p>
                      <a href="{{ url(action('PostsController@show', $post->slug)) }}" class="btn btn-1">Read More</a> </section>
              </div>
            </li>
            <?php $category = Category::where('slug', '=', 'nature')->first(); 
              $post = $category->posts()->orderBy('id','desc')->first();
            ?>
            <li class="span3">
                <div class="thumbnail thumbnail-1">
                <h3>{{ $category->name_th }}</h3>
                <img  src="{{ url($post->cover->url('large')) }}" alt="{{ $post->title}}">
                <section> <strong>{{ $post->title}} </strong>
                      <p>{{ $post->summary }}</p>
                      <a href="{{ url(action('PostsController@show', $post->slug)) }}" class="btn btn-1">Read More</a> </section>
              </div>
            </li>
            <?php $category = Category::where('slug', '=', 'love-story')->first(); 
              $post = $category->posts()->orderBy('id','desc')->first();
            ?>
            <li class="span3">
                <div class="thumbnail thumbnail-1">
                <h3>{{ $category->name_th }}</h3>
                <img  src="{{ url($post->cover->url('large')) }}" alt="{{ $post->title}}">
                <section> <strong>{{ $post->title}} </strong>
                      <p>{{ $post->summary }}</p>
                      <a href="{{ url(action('PostsController@show', $post->slug)) }}" class="btn btn-1">Read More</a> </section>
              </div>
            </li>
            <?php $category = Category::where('slug', '=', 'fine-art')->first(); 
              $post = $category->posts()->orderBy('id','desc')->first();
            ?>
            <li class="span3">
                <div class="thumbnail thumbnail-1">
                <h3>{{ $category->name_th }}</h3>
                <img  src="{{ url($post->cover->url('large')) }}" alt="{{ $post->title}}">
                <section> <strong>{{ $post->title}} </strong>
                      <p>{{ $post->summary }}</p>
                      <a href="{{ url(action('PostsController@show', $post->slug)) }}" class="btn btn-1">Read More</a> </section>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
@extends('layouts.application')

@section('title')
{{$post->title}}
@stop

@section('style')
<style type="text/css">
.portfolio li {
float: left;
margin: 0 0 30px 30px;
max-width: 190px;
}
{{$post->style}}
</style>


@stop

@section('meta')
    <meta name="description" content="{{ Setting::SITE_META() }}">
    <meta name="author" content="Wattana Bumrungsawat">
    <link rel="canonical" href="http://www.bumrungsawat.com/2014/" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $post->title }}" />
    <meta property="og:description" content="{{ $post->summary }}" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:site_name" content="Bumrungsawat" />
    <meta property="article:publisher" content="https://www.facebook.com/hansamurano" />
    <meta property="og:image" content="{{ url($post->cover->url('medium')) }}" />
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@hansamurano"/>
    <meta name="twitter:domain" content="Bumrungsawat"/>
    <meta name="twitter:creator" content="/@hansamurano"/>
@stop

@section('main')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/th_TH/sdk.js#xfbml=1&appId=1428532447420222&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="bg-content">       
  <!--============================== content =================================-->      
   <div id="content">
    <div class="container">
      <div class="row">
        <article class="span8">
         <div class="inner-1">         
            <h3>{{ $post->title }}</h3>     
            <time datetime="{{ date("d.m.Y",strtotime($post->created_at)) }}" class="date-1">{{ date("d.m.Y",strtotime($post->created_at)) }}</time>
            <div class="name-author">by <a href="#">{{ $post->user->username }}</a></div>    
            <div class="clear"></div>    
            <p>{{ $post->content }}</p>
            <ul class="portfolio clearfix">       
            @foreach($post->images as $image)    
              <li class="box"><a href="{{url($image->image->url('original'))}}" class="magnifier" ><img alt="" alt="{{ $post->title }}" src="{{url($image->image->url('large'))}}"></a></li>      
            @endforeach                
            </ul> 
          </div>  
            <div class="fb-like" data-href="{{ Request::url() }}" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
            <hr>

            <!-- the comment box -->
            <div class="fb-comments" data-href="{{ Request::url() }}" data-width="770" data-numposts="10" data-colorscheme="dark"></div>    
        </article>
        <article class="span4">
          <h3>Categories</h3>
          <ul class="list extra extra1">           
            <?php $categories = Category::where('category_id', '0')->orderBy('id', 'desc')->get(); ?>
            <li><a href="{{ action('PostsController@index') }}">All</a></li>
            @foreach($categories as $category)
            <li><a href="{{ action('PostsController@index_category', array("category_id"=> $category->slug)) }}">{{ $category->name_th }} ({{$category->posts()->count()}})</a></li>
            @endforeach                 
          </ul>
          
        </article>
      </div>
     </div>
   </div>
</div>
@stop

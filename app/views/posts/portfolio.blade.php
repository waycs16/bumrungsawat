@section('style')
<style type="text/css">
/**** Isotope CSS3 transitions ****/

.isotope,
.isotope .isotope-item {
  -webkit-transition-duration: 0.8s;
     -moz-transition-duration: 0.8s;
      -ms-transition-duration: 0.8s;
       -o-transition-duration: 0.8s;
          transition-duration: 0.8s;
}

.isotope {
  -webkit-transition-property: height, width;
     -moz-transition-property: height, width;
      -ms-transition-property: height, width;
       -o-transition-property: height, width;
          transition-property: height, width;
}

.isotope .isotope-item {
  -webkit-transition-property: -webkit-transform, opacity;
     -moz-transition-property:    -moz-transform, opacity;
      -ms-transition-property:     -ms-transform, opacity;
       -o-transition-property:         top, left, opacity;
          transition-property:         transform, opacity;
}

/**** disabling Isotope CSS3 transitions ****/

.isotope.no-transition,
.isotope.no-transition .isotope-item,
.isotope .isotope-item.no-transition {
  -webkit-transition-duration: 0s;
     -moz-transition-duration: 0s;
      -ms-transition-duration: 0s;
       -o-transition-duration: 0s;
          transition-duration: 0s;
}

/* End: Recommended Isotope styles */



/* disable CSS transitions for containers with infinite scrolling*/
.isotope.infinite-scrolling {
  -webkit-transition: none;
     -moz-transition: none;
      -ms-transition: none;
       -o-transition: none;
          transition: none;
}

.element {
  width: 290px;
  height: auto;
  margin:5px 10px 5px 0;
  float: left;
  overflow: hidden;
  position: relative;
  color: #222;
}
 #filters.pagination li a.selected{
background: #e85356;
color: #fff;
  }
.pagination a, .pagination span {
color: #e85356;
background-color: #303030;
border: 1px solid #dddddd;
font-family: 'Open Sans', sans-serif;
}

.pagination a:hover, .pagination .active a, .pagination .active span {
background: #e85356;
color: #fff;
}
.tm_pad{
  padding-top: 50px;
  padding-bottom: 30px;
}
.magnify {
  opacity: 1;
  position: relative;
  height: auto;
  display: block;
  -webkit-transition: all 0.3s ease;
  -moz-transition: all 0.3s ease;
  -o-transition: all 0.3s ease;
  transition: all 0.3s ease;
}
.magnify:hover {
  opacity: 0.7;
}
.magnify:after {
  content: '';
  display: block;
  width: 100%;
  height: 100%;
  position: absolute;
  opacity: 0;
  left: 0;
  top: 0;
  background: url('{{ url("/img/magnifier.png") }}') center center no-repeat #000000;
  -webkit-transition: all 0.3s ease;
  -moz-transition: all 0.3s ease;
  -o-transition: all 0.3s ease;
  transition: all 0.3s ease;
}
.magnify:hover:after {
  opacity: 0.6;
}
</style>
@stop

        <?php 
        $posts = null;
        if($category && $category->slug == "photography") 
        {
          $category_sub = Category::where('category_id', '=', $category->id)->get();

          $post_ids = array();
          foreach($category_sub as $sub_cat)
          {
            $post_ids = array_merge($post_ids, $sub_cat->posts()->lists('post_id'));
          }
          $posts = Post::whereIn('id', $post_ids)->orderBy('id', 'desc')->get();
        }
        else if($category->parent && $category->parent->slug == "photography")
        {          
          $post_ids = $category->posts()->lists('post_id');
          
          $posts = Post::whereIn('id', $post_ids)->orderBy('id', 'desc')->get();
        }

        ?>
        <article class="span12">
        <h3>Portfolio</h3>
         </article>
         <div class="clear"></div>
         <div id="options" class="clearfix" style="margin-left:20px;">
            <ul id="filters" class="pagination option-set clearfix" data-option-key="filter">
              <li><a href="#filter" data-option-value="*" class="selected">show all</a></li>
              <li><a href="#filter" data-option-value=".fashion">Fashion</a></li>
              <li><a href="#filter" data-option-value=".nature">Nature</a></li>
              <li><a href="#filter" data-option-value=".love-story">Love Story</a></li>
              <li><a href="#filter" data-option-value=".fine-art">Fine Art</a></li>
            </ul>
        </div> <!-- #options -->
         <div id="portfolio" class="portfolio clearfix">    
          @foreach($posts as $post)     
            <div class="box element {{ $post->isotope() }} span4" data-category="transition">
              <a href="{{ url(action('PostsController@show', $post->slug)) }}" title="{{ $post->title }}" class="magnify">
                <img alt="" src="{{ url($post->cover->url('large')) }}">
              </a>
            </div>          
          @endforeach  
          </div> 

@section('script')       
    {{ HTML::script('assets/js/jquery.isotope.min.js') }}
<script type="text/javascript">
$(window).load(function(){
    
    var $container = $('#portfolio');

    $container.isotope({
      itemSelector : '.element'
    });
    
    
    var $optionSets = $('#options .option-set'),
        $optionLinks = $optionSets.find('a');

    $optionLinks.click(function(){
      var $this = $(this);
      // don't proceed if already selected
      if ( $this.hasClass('selected') ) {
        return false;
      }
      var $optionSet = $this.parents('.option-set');
      $optionSet.find('.selected').removeClass('selected');
      $this.addClass('selected');

      // make option object dynamically, i.e. { filter: '.my-filter-class' }
      var options = {},
          key = $optionSet.attr('data-option-key'),
          value = $this.attr('data-option-value');
      // parse 'false' as false boolean
      value = value === 'false' ? false : value;
      options[ key ] = value;
      if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
        // changes in layout modes need extra logic
        changeLayoutMode( $this, options )
      } else {
        // otherwise, apply new options
        $container.isotope( options );
      }
      
      return false;
    });

    
  });
</script>
@stop   
@extends('layouts.application')

@section('title')
ข่าวสารและสาระน่ารู้
@stop

@section('meta')
    <meta name="description" content="{{ Setting::SITE_META() }}">
    <meta name="author" content="Wattana Bumrungsawat">
    <link rel="canonical" href="http://www.bumrungsawat.com/2014/" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ Setting::SITE_TITLE() }}" />
    <meta property="og:description" content="{{ Setting::SITE_META() }}" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:site_name" content="Bumrungsawat" />
    <meta property="article:publisher" content="https://www.facebook.com/hansamurano" />
    <meta property="og:image" content="http://www.bumrungsawat.com/2014/assets/images/logo.png" />
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@hansamurano"/>
    <meta name="twitter:domain" content="Bumrungsawat"/>
    <meta name="twitter:creator" content="/@hansamurano"/>
@stop

@section('main')
<div class="bg-content">       
  <!--============================== content =================================-->      
   <div id="content">
    <div class="container">
      <div class="row">
        @if((isset($category)) && (($category && $category->slug == "photography") || ($category && $category->parent && $category->parent->slug == "photography")))
        <?php echo View::make('posts.portfolio', compact('category')); ?>
        @else
        <article class="span8">
         <div class="inner-1">         
          <ul class="list-blog">
            @foreach($posts as $post)
            <li>  
                <h3>{{ $post->title }}</h3>     
                <time datetime="{{ date("d.m.Y",strtotime($post->created_at)) }}" class="date-1">{{ date("d.m.Y",strtotime($post->created_at)) }}</time>
                <div class="name-author">by <a href="#">{{ $post->user->username }}</a></div>    
                <div class="clear"></div>    
                <img alt="" src="{{ url($post->cover->url('large')) }}">  
                <p>{{ $post->summary }}</p>
                <a href="{{ url(action('PostsController@show', $post->slug)) }}" class="btn btn-1">Read More</a>          
            </li> 
            @endforeach      
          </ul>
          </div>  
          {{ $posts->links()}}
        </article>
        <article class="span4">
          <h3>Categories</h3>
          <ul class="list extra extra1">           
            <?php $categories = Category::where('category_id', '0')->orderBy('id', 'desc')->get(); ?>
            <li><a href="{{ action('PostsController@index') }}">All</a></li>
            @foreach($categories as $category)
            <?php $count = $category->posts()->count(); 
              $category_sub = Category::where('category_id', '=', $category->id)->get();
              foreach ($category_sub as $subCategory) {
                $count = $count + $subCategory->posts()->count();
              }
            ?>
            <li><a href="{{ action('PostsController@index_category', array("category_id"=> $category->slug)) }}">{{ $category->name_th }} ({{$count}})</a></li>
            @endforeach                 
          </ul>
          
        </article>
        @endif
      </div>
     </div>
   </div>
</div>

@stop

@section('script')	

@stop
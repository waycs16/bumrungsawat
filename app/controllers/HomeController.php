<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		$images = Image::orderBy('id', 'desc')->take(12)->get();
		$category = Category::where('slug', '=', 'talk')->first(); 
    $talk = $category->posts()->orderBy('id','desc')->first();
		return View::make('home', compact('images', 'talk'));
	}

}
